# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.2.4

- patch: Internal maintenance: Bump base docker image to 2.70.0.
- patch: Internal maintenance: Update pipes versions in pipelines configuration file.

## 1.2.3

- patch: Internal maintenance: Bump pipes versions in pipelines configuration file.
- patch: Internal maintenance: Update docker image to azure-cli:2.67.0

## 1.2.2

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 1.2.1

- patch: authentication command updated in pipe.sh

## 1.2.0

- minor: Bump azure-cli docker image to version 2.53.0.

## 1.1.0

- minor: Update azure-cli image to version 2.40.0.
- patch: Internal maintenance: update bitbucket-pipes-toolkit-bash.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update default image, pipe versions in bitbucket-pipelines.yml.

## 1.0.1

- patch: Internal maintenance: make new pipe consistent to others.

## 1.0.0

- major: Note: This pipe was forked from https://bitbucket.org/microsoft/azure-cli-run for future maintenance purposes.
